import javax.print.DocFlavor;
import javax.sound.midi.Sequence;
import java.sql.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {


    private double realPartA;
    private double imaginaryPartI;
    private double imaginaryPartJ;
    private double imaginaryPartK;
    public static final double EPSILON = 1E-6;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        realPartA = a;
        imaginaryPartI = b;
        imaginaryPartJ = c;
        imaginaryPartK = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {

        return realPartA;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {

        return imaginaryPartI;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return imaginaryPartJ;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return imaginaryPartK;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        List<String> strlist = new ArrayList<>(Arrays.asList("", "i", "j", "k"));
        return Arrays.stream(new double[]{realPartA, imaginaryPartI, imaginaryPartJ, imaginaryPartK})
                .mapToObj(x -> x >= 0 ? "+" + (int) x : String.valueOf((int) x))
                .map(x -> {
                    x += strlist.get(0);
                    strlist.remove(0);
                    return x;
                }).collect(Collectors.joining());
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        if (s == null || s.isEmpty()) throw new RuntimeException(String.format("%s input cannot be null or empty", s));

        List<String> tmpStream = new ArrayList<>(Arrays.asList(s.split("\\+|(?=-)")))
                .stream()
                .filter(x -> !x.isEmpty())
                .collect(Collectors.toList());

<<<<<<< HEAD:H4/src/Quaternion.java
=======
        double[] doubles = new double[4];
        System.out.println(Arrays.toString(doubles));
        int counter = 0;
>>>>>>> 914e22a78a122516bac8c27fa2b9b7123881fcda:homework4/src/Quaternion.java

        double[] doubles = new double[ 4 ];
        System.out.println(Arrays.toString(doubles));
        int counter = 0;
        for (String str : tmpStream) {
            System.out.println(str);
<<<<<<< HEAD:H4/src/Quaternion.java
            if (counter == 0){
                doubles[counter] = Double.parseDouble(str);
            }
            else if(str.indexOf("i") == str.lastIndexOf("i")){
                doubles[counter] = Double.parseDouble(str.replaceAll("[^-\\d]", ""));
            }
            else if(str.indexOf("j") == str.lastIndexOf("j")){
                doubles[counter] = Double.parseDouble(str.replaceAll("[^-\\d]", ""));

            }else if(str.indexOf("k") == str.lastIndexOf("k")){
                doubles[counter] = Double.parseDouble(str.replaceAll("[^-\\d]", ""));
            }else {
                throw new IllegalArgumentException(String.format("wrong input representation for %s", s));
=======
            if (counter == 0) {
                doubles[counter] = Double.parseDouble(str.replaceAll("[^-\\d]", ""));
                counter++;
                continue;
            }

            if (str.contains("i")) {
                if (str.indexOf("i") == str.lastIndexOf("i") && counter == 1) {
                    doubles[counter] = Double.parseDouble(str.replaceAll("[^-\\d]", ""));
                    counter++;
                    continue;
                }
                throw new RuntimeException(String.format("Wrong value slot for %s in input string %s", str, s));
            }

            if (str.contains("j")) {
                if (str.indexOf("j") == str.lastIndexOf("j") && counter == 2) {
                    doubles[counter] = Double.parseDouble(str.replaceAll("[^-\\d]", ""));
                    counter++;
                    continue;
                }
                throw new RuntimeException(String.format("Wrong value slot for %s in input string %s", str, s));
            }

            if (str.contains("k")) {
                if (str.indexOf("k") == str.lastIndexOf("k") && counter == 3) {
                    doubles[counter] = Double.parseDouble(str.replaceAll("[^-\\d]", ""));
                }
                throw new RuntimeException(String.format("Wrong value slot for %s in input string %s", str, s));
            } else {
                throw new RuntimeException(String.format("wrong input representation for %s", s));
>>>>>>> 914e22a78a122516bac8c27fa2b9b7123881fcda:homework4/src/Quaternion.java
            }
            counter++;
        }
<<<<<<< HEAD:H4/src/Quaternion.java
        return new Quaternion(doubles[ 0 ], doubles[ 1 ], doubles[ 2 ], doubles[ 3 ]);
    }

    public static boolean isInteger ( String str ) {
        return str.matches("^-?[0-9]+(\\.[0-9]+)?$");
=======
        return new Quaternion(doubles[0], doubles[1], doubles[2], doubles[3]);
>>>>>>> 914e22a78a122516bac8c27fa2b9b7123881fcda:homework4/src/Quaternion.java
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {

        Quaternion tmpQuat = new Quaternion(realPartA, imaginaryPartI, imaginaryPartJ, imaginaryPartK);
        return tmpQuat;
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        double sum = 0;
        if (Math.abs(realPartA) > EPSILON) {
            sum += Math.abs(realPartA);
        }
        if (Math.abs(imaginaryPartI) > EPSILON) {
            sum += Math.abs(imaginaryPartI);
        }
        if (Math.abs(imaginaryPartJ) > EPSILON) {
            sum += Math.abs(imaginaryPartJ);
        }
        if (Math.abs(imaginaryPartK) > EPSILON) {
            sum += Math.abs(imaginaryPartK);
        }
        return sum == 0;
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(realPartA, imaginaryPartI * -1, imaginaryPartJ * -1, imaginaryPartK * -1);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(realPartA * -1, imaginaryPartI * -1, imaginaryPartJ * -1, imaginaryPartK * -1);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(
                this.realPartA + q.realPartA,
                this.imaginaryPartI + q.imaginaryPartI,
                this.imaginaryPartJ + q.imaginaryPartJ,
                this.imaginaryPartK + q.imaginaryPartK);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        double realNum = this.realPartA * q.realPartA - this.imaginaryPartI * q.imaginaryPartI - this.imaginaryPartJ * q.imaginaryPartJ - this.imaginaryPartK * q.imaginaryPartK;
        double imaginaryI = this.realPartA * q.imaginaryPartI + this.imaginaryPartI * q.realPartA + this.imaginaryPartJ * q.imaginaryPartK - this.imaginaryPartK * q.imaginaryPartJ;
        double imaginaryJ = this.realPartA * q.imaginaryPartJ - this.imaginaryPartI * q.imaginaryPartK + this.imaginaryPartJ * q.realPartA + this.imaginaryPartK * q.imaginaryPartI;
        double imaginaryK = this.realPartA * q.imaginaryPartK + this.imaginaryPartI * q.imaginaryPartJ - this.imaginaryPartJ * q.imaginaryPartI + this.imaginaryPartK * q.realPartA;
        return new Quaternion(realNum, imaginaryI, imaginaryJ, imaginaryK);
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(
                this.realPartA * r,
                this.imaginaryPartI * r,
                this.imaginaryPartJ * r,
                this.imaginaryPartK * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        double realNum = inverseHelper();
        double imaginaryI = inverseHelper();
        double imaginaryJ = inverseHelper();
        double imaginaryK = inverseHelper();
        if (Math.abs(realNum) < EPSILON || Math.abs(imaginaryI) < EPSILON || Math.abs(imaginaryJ) < EPSILON || Math.abs(imaginaryK) < EPSILON) {
            throw new RuntimeException("Cannot divide with 0 with this Quaternion " + this);
        }
        return new Quaternion(realPartA / realNum, -imaginaryPartI / imaginaryI, -imaginaryPartJ / imaginaryJ, -imaginaryPartK / imaginaryK);
    }

    private double inverseHelper() {
        return (Math.pow(realPartA, 2) + Math.pow(imaginaryPartI, 2) + Math.pow(imaginaryPartJ, 2) + Math.pow(imaginaryPartK, 2));
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return new Quaternion(
                this.realPartA - q.realPartA,
                this.imaginaryPartI - q.imaginaryPartI,
                this.imaginaryPartJ - q.imaginaryPartJ,
                this.imaginaryPartK - q.imaginaryPartK);
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        try {
            return this.times(q.inverse());
        } catch (RuntimeException e) {
            throw new RuntimeException(String.format("Can't perform divideByRight Between %s and %s, check for null dividing operators", this, q));
        }
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        try {
            return q.inverse().times(this);
        } catch (RuntimeException e) {
            throw new RuntimeException(String.format("Can't perform divideByLeft Between %s and %s, check for null dividing operators", this, q));
        }
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param obj second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Quaternion other = (Quaternion) obj;
        if (Math.abs(this.realPartA - other.realPartA) > EPSILON) return false;
        if (Math.abs(this.imaginaryPartI - other.imaginaryPartI) > EPSILON) return false;
        if (Math.abs(this.imaginaryPartJ - other.imaginaryPartJ) > EPSILON) return false;
        if (Math.abs(this.imaginaryPartK - other.imaginaryPartK) > EPSILON) return false;
        return true;
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        Quaternion tmp = this.times(q.conjugate()).plus(q.times(conjugate()));
        return new Quaternion(tmp.realPartA / 2, tmp.imaginaryPartI / 2, tmp.imaginaryPartJ / 2, tmp.imaginaryPartK / 2);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        // Voimalikult abstraktsed arvud, et ainult samal kombinatsioonil tuleks sama HashCode, muidu on erinevad.
        int result = 53;
        result = (int) (result * 15 * this.realPartA * 50 + result * 15 + this.imaginaryPartI * 60 + result * 15 * imaginaryPartJ + 75 + result * 15 * imaginaryPartK * 100);
        return result;
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(Math.pow(realPartA, 2) + Math.pow(imaginaryPartI, 2) + Math.pow(imaginaryPartJ, 2) + Math.pow(imaginaryPartK, 2));
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {

<<<<<<< HEAD:H4/src/Quaternion.java
        //System.out.println(valueOf("+2+5i+7j+9k"));
        //System.out.println(q1.hashCode());

        Quaternion f = new Quaternion(2., 5., 7., 9.);
        //System.out.println(f);
        valueOf("+2+5i+7j+9k");
=======

        System.out.println(valueOf("-1+0i+0j+4k"));
        //System.out.println(q1.hashCode());
        Quaternion f = new Quaternion(-1., 0., 0., 4.);
        System.out.println(f);
        //Quaternion g = new Quaternion(8., 10., 11., 20.);
>>>>>>> 914e22a78a122516bac8c27fa2b9b7123881fcda:homework4/src/Quaternion.java
//        System.out.println(f.hashCode());
//        System.out.println(d.hashCode());
//        System.out.println(g.hashCode());
    }
}
// end of file
