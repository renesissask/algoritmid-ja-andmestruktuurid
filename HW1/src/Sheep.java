
public class Sheep {

    enum Animal {sheep, goat}

    public static void main(String[] param) {
    }

    public static void reorder(Animal[] animals) {
        int goatCount = 0;

        for (Animal animal : animals) {
            goatCount += animal == Animal.goat ? 1 : 0;
        }

        if(goatCount == 0) {
            return;
        }

        for (int i = 0; i < animals.length; i++) {
            animals[i] = i < goatCount ? Animal.goat : Animal.sheep;
        }
    }
}
