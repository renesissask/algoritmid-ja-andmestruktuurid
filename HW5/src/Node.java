import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;
    private int depth = 0;

    Node ( String n, Node child, Node sibling ) {
        this.name = n;
        this.firstChild = child;
        this.nextSibling = sibling;
    }

    public static Node parsePostfix ( String s ) {
        Pattern pattern = Pattern.compile("\\(,.\\)|\\(.,\\)|,{2,}|\\s|\\t|\\(\\)");
        Matcher matcher = pattern.matcher(s);
        if ( matcher.find() ) {
            throw new RuntimeException(String.format("Wrong input for parsePostFix: %s,  not supported value pair: %s", s, matcher.group(0)));
        }
        StringTokenizer tokens = new StringTokenizer(s, "(),", true);
        Stack<Node> nodeStack = new Stack<>();
        while ( tokens.hasMoreTokens() ) {
            String tok = tokens.nextToken();
            if ( tok.equals("(") ) {
                continue;
            }

            if ( tok.equals(")") ) {
                Node newNode = new Node(tokens.nextToken(), nodeStack.pop(), null);
                nodeStack.push(newNode);
                continue;
            }
            if ( tok.equals(",") ) {
                String tmptok = tokens.nextToken();
                if ( tmptok.equals("(") ) {
                    while ( true ) {
                        tmptok = tokens.nextToken();
                        if ( tmptok.equals(")") ) {
                            Node newNode = new Node(tokens.nextToken(), nodeStack.pop(), null);
                            nodeStack.push(newNode);
                            break;
                        } else if ( tmptok.equals(",") ) {
                            String tmpTok = tokens.nextToken();
                            Node tmpNode = nodeStack.pop();
                            tmpNode.addLastSibling(new Node(tmpTok, null, null));
                            nodeStack.push(tmpNode);
                        } else {
                            nodeStack.push(new Node(tmptok, null, null));
                        }
                    }
                    Node sib = nodeStack.pop();
                    Node parent = nodeStack.pop();
                    parent.addLastSibling(sib);
                    nodeStack.push(parent);
                    continue;
                }
                Node tmpNode = nodeStack.pop();
                tmpNode.addLastSibling(new Node(tmptok, null, null));
                nodeStack.push(tmpNode);
            } else {
                nodeStack.push(new Node(tok, null, null));
            }
        }
        if ( nodeStack.size() != 1 ) {
            throw new RuntimeException(String.format("Should not have double brackets %s", s));
        }
        if ( nodeStack.peek().nextSibling != null ) {
            throw new RuntimeException(String.format("Root Should not have sibling %s", s));
        }
        return nodeStack.pop();
    }

    /*
    Kuna ei teadnud, et 17ndal ei toimunud kaitsmist ja praktikumi osa, seega õigeaegselt kohale jõudes, tuli välja, et ei olnud ainukene, kes infokillust ilma jäänud
    ja tegin kohapeal eksinud kaaskursuslasega Mihkel Riik parse_XML ja testide tegemise osa koos ära, mille tegemisega läks meil kohapeal 3 tundi.
    Kuna me juba arutasime kohapeal, et kuidas oleks antud probleemi kõige parem lahendada, siis on väga keeruline originaalse idee peale tulla kui juba on selline mall
    silme ees
     */
    public String pseudo_XML () {
        if ( this.name == null ) {
            throw new RuntimeException("Name cannot be null");
        }
        return pseudo_XML(1);
    }

    private String pseudo_XML ( int depth ) {

        StringBuilder st = new StringBuilder();

        st.append("<L").append(depth).append("> ");
        st.append(this);
        if ( !( this.hasChild() ) ) {
            st.append(" ");
        }
        if ( this.hasChild() ) {

            st.append("\n");

            st.append("\t".repeat(depth));
            st.append(this.firstChild.pseudo_XML(depth + 1));
            if ( this.firstChild.hasSibling() ) {
                Node child = this.firstChild;
                while ( ( child.hasSibling() ) ) {
                    if ( depth == 1 ) {
                        st.append("\t".repeat(depth));
                    } else {
                        st.append("\t".repeat(depth - ( depth - 1 )));
                    }
                    st.append(child.nextSibling.pseudo_XML(depth + 1));
                    child = child.nextSibling;
                }
            }
        }
        st.append("</L").append(depth).append(">");
        if ( depth != 1 ) {
            st.append("\n");
        }
        if ( depth != 1 ) {
            st.append("\t".repeat(depth - 2));
        }
        return st.toString();
    }


    public void addLastSibling ( Node a ) {
        if ( nextSibling == null ) {
            nextSibling = a;
            return;
        }
        nextSibling.addLastSibling(a);
    }


    //https://git.wut.ee/i231/home5/src/branch/master/src/Node.java
    // alloleva meetodi viide.
    public String leftParentheticRepresentation () {
        if ( this.name == null ) {
            throw new RuntimeException("Cant represent null Node");
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name);
        if ( hasChild() ) {
            stringBuilder.append("(");
            stringBuilder.append(firstChild.leftParentheticRepresentation());
            stringBuilder.append(")");
        }
        if ( hasSibling() ) {
            stringBuilder.append(",");
            stringBuilder.append(nextSibling.leftParentheticRepresentation());
        }
        return stringBuilder.toString();
    }

    public boolean hasChild () {
        return firstChild != null;
    }

    public boolean hasSibling () {
        return nextSibling != null;
    }

    @Override
    public String toString () {
        return name;
    }

    public static void main ( String[] param ) {

//        Node n = new Node("Node1", null, null);
//        Node n2 = new Node("Node2", null, null);
//        Node n3 = new Node("Node3", null, null);
//        n.addChild(n2);
//        n.addChild(n3);
//        System.out.println(n.getLastChild());
//        System.out.println(n.getLastSibling());
//        //n.addSibling(new Node("node3", null,null));
//


        String s = "(AAA,)A";
        Node t = Node.parsePostfix(s);
        System.out.println(t);
        System.out.println(t.leftParentheticRepresentation());
//        System.out.println(t.leftParentheticRepresentation());

    }
}


