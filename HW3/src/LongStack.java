import java.util.*;
import java.util.stream.Collectors;

public class LongStack {

    public static void main(String[] argum) {

        LongStack stack = new LongStack();

        stack.push(1);
        stack.push(2);
<<<<<<< HEAD
        stack.push(3);
        System.out.println(interpret("2 5 9 ROT + SWAP -"));
=======
        System.out.println(stack.longStackList);
        stack.op("SWAP");
        System.out.println(interpret("SWAP 1 6"));
>>>>>>> f236e5e9cfd453de2923655c813e5762ac5554c4
    }


    private final LinkedList<Long> longStackList;

    LongStack() {
        longStackList = new LinkedList<>();
    }

    // Shallow copy
    @Override
    public Object clone() throws CloneNotSupportedException {
        //Object clone = super.clone();
        LongStack newStack = new LongStack();
        newStack.longStackList.addAll(this.longStackList);
        return newStack;
    }

    public boolean stEmpty() {
        return longStackList.isEmpty();
    }

    public void push(long a) {
        longStackList.add(a);
    }


    public long pop() {
        if (longStackList.size() <= 0) {
            throw new IllegalArgumentException("Can't remove what's not in there, Stack underflow");
        }
        return longStackList.remove(longStackList.size() - 1);
    }

<<<<<<< HEAD
    public void op ( String s ) {
        if( s.equals("ROT") ){
            long element1 = pop();
            long element2 = pop();
            long element3 = pop();
            push(element2);
            push(element1);
            push(element3);
            return;
        }
        if ( longStackList.size() < 2 ) {
=======
    public void op(String s) {
        if (s.equals("ROT") && longStackList.size() < 3) {
            throw new IllegalArgumentException(String.format("Not enough elements for %s operation", s));
        }
        if (longStackList.size() < 2) {
>>>>>>> f236e5e9cfd453de2923655c813e5762ac5554c4
            throw new IllegalArgumentException(String.format("Not enough elements for %s operation", s));
        }

        long element1 = pop();
        long element2 = pop();

        switch (s) {
            case "*":
                push(element2 * element1);
                break;
            case "/":
                push(element2 / element1);
                break;
            case "+":
                push(element2 + element1);
                break;
            case "-":
                push(element2 - element1);
                break;
            case "SWAP":
                push(element1);
                push(element2);
                break;
<<<<<<< HEAD
=======
            case "ROT":
                long element3 = pop();
                push(element2);
                push(element1);
                push(element3);
                break;
>>>>>>> f236e5e9cfd453de2923655c813e5762ac5554c4
            default:
                throw new IllegalArgumentException(String.format("Unsupported operator %s", s));
        }
    }

    public long tos() {
        if (longStackList.size() <= 0) {
            throw new IllegalArgumentException("Can't check top of stack if stack is empty u doofus, Stack Underflow");
        }
        return longStackList.get(longStackList.size() - 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;

        }
        LongStack other = (LongStack) obj;
        if (other.longStackList.size() != this.longStackList.size()) {
            return false;
        }
        for (int i = 0; i < other.longStackList.size(); i++) {
            if (!this.longStackList.get(i).equals(other.longStackList.get(i))) {
                return false;
            }
        }
        return true;
    }

    /*
     Sooviks tagasisides teada, et kuivõrd kiirem on StringBuilder näiteks
     streami Collectors.joining methodist(näide allpool toString lõpus)
     */
    @Override
    public String toString() {

        StringBuilder str = new StringBuilder();
        for (Long value : longStackList) {
            str.append(value).append(" ");
        }
        return str.toString();

//        return longStackList
//                .stream()
//                .map(Object::toString)
//                .collect(Collectors.joining(" "));
    }

    public static long interpret(String pol) {
        if (pol == null || pol.isEmpty()) {
            throw new RuntimeException("Empty expression");
        }

        List<String> polElements = new ArrayList<>(Arrays.asList(pol.split(" ")))
                .stream()
                .map(String::trim)
                .filter(x -> !x.isEmpty())
                .collect(Collectors.toList());
        System.out.println(polElements);

        List<String> supportedOperators = new ArrayList<>(Arrays.asList("+", "-", "*", "/", "ROT", "SWAP"));
        LongStack stack = new LongStack();

        for (String value : polElements) {
            if (!isNumeric(value) && !supportedOperators.contains(value)) {
                throw new RuntimeException(String.format("Illegal symbol \"%s\" in experssion \"%s\"", value, pol));
            }
            if (isNumeric(value)) {
                stack.push(Long.parseLong(value));
                continue;
            }
            if (stack.longStackList.size() < 3 && value.equals("ROT")){
                throw new RuntimeException(String.format("Cannot perform %s in expression \"%s\"", value, pol));
            }
            if (stack.longStackList.size() <= 1) {
                throw new RuntimeException(String.format("Cannot perform %s in expression \"%s\"", value, pol));
            }
            stack.op(value);
        }
        if (stack.longStackList.size() > 1) {
            throw new RuntimeException(String.format("Too many numbers in expression \"%s\"", pol));
        }
        return stack.tos();
    }

    private static boolean isNumeric(String num) {
        try {
            Long.parseLong(num);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}

